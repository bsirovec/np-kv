// DPL sa pokazivacem na prvi element

//Funkcije:
// + 1.za inicijalizaciju ( dinamički alocirane memorija za listu ) ++
//  2.brisanje list
// +  3.append (kraj) 
// + 4.brisanje n-tog elementa iz liste
//  5.brisanje svih elemenata iz list
// +  6.ispis elemanata iz liste 
// +  7.isEmpty()
// 
//  8.sort


// Modul izvesti kao dinamicku biblioteku

#include <stdio.h>
#include <stdlib.h>


struct Node {
    int data;
    struct Node* next;
    struct Node* prev;
};

struct List{
    struct Node* head;
    int capacity;
    char *name;
};

struct List* initializeList(int,char*);
void deleteList();
void append(struct List*, int);
void deleteNode(struct List*, int);
int empty();
void print(struct List*);
int calculateSize(struct List*);
void isEmpty(struct List*);
int isFull(struct List*);

//1
struct List *initializeList(int capacity, char* name){
    struct List *list = (struct List*)malloc(sizeof(struct List));
    list->head = NULL;
    list->capacity = capacity;
    list->name = name;
    return list;
}


//2
void deleteList();

//3
void append(struct List* lista, int to_append){
    if(isFull(lista)==1){
        printf("Lista puna, append ne prolazi \n");
        return;
    }

    //inicijaliziraj memoriju
    struct Node* new_node = (struct Node*)malloc(sizeof(struct Node));
    struct Node* last = lista->head;

    //novi node ima vrijednost data i next mu je null ( jer ide na kraj )
    new_node->data = to_append;
    new_node->next = NULL;

    //ako ne postoji niti jedan, novi node je prvi
    //head pokazuje na nas node, prev našeg nodea je null
    if(lista->head == NULL){
        new_node->prev = NULL;
        lista->head = new_node;
        return;
    }

    //ako postoji elementi, iteriraj dok ne dodes do zadnjeg
    //zadnjem je next == NULL
    while(last->next != NULL)
        last = last->next;

    //last->next pokazuje na new_node
    //last je sad predzadnji, new_node zadnji
    //new_node->prev pokazuje na zadnji
    last->next = new_node;
    new_node->prev = last;

    return;
}

//4
void deleteNode(struct List* lista, int to_delete){
    int i;
    struct Node* node_to_delete;
    struct Node* help_node;
    node_to_delete = lista->head;

    if(lista->head == NULL)
        return;

    //brisanje heada
    if (to_delete==0){
        lista->head = lista->head->next;
        lista->head->next->prev = NULL;}
    
    if(to_delete==lista->capacity-1){
        for(i=0;i<to_delete;i++){
            node_to_delete = node_to_delete->next;
        }
        help_node = node_to_delete->prev;
        help_node->next=NULL;
        node_to_delete->prev = NULL;
    }
    else{
        for(i=0;i<to_delete;i++){
            node_to_delete = node_to_delete->next;
        }
        help_node = node_to_delete->prev;
        help_node->next = node_to_delete ->next;
        node_to_delete->next->prev = help_node;
    }



    free(node_to_delete);
    return;
}
//5
int empty();
//6
void print(struct List* list){
    struct Node* node = list->head;
    //last označava zadnji node
    struct Node* last;
    //nakon fwd printa last koristimo "unazad"
    printf("\n Fwd direction \n");
    while(node != NULL){
        printf(" %d ", node->data);
        last = node;
        node = node->next;
    }

    /* printf("\n Reverse direction \n");
    while(last != NULL){
        printf(" %d ", last->data);
        last=last->prev;
    } */

    return;
};
//7
int calculateSize(struct List* list){
    struct Node* node = list->head;
    int counter=0;
    while(node!=NULL){
        counter++;
        node=node->next;
    }

    return counter;
}

void isEmpty(struct List* list){
    int size;
    struct Node* node = list->head;
    if(node!=NULL)
        printf("\n List is not empty \n");
        size = calculateSize(list);
        if(size==0){printf("List is empty \n");}
        else 
            printf(" List size: %d \n", size);
        return;

    
    return;
};

int isFull(struct List* list){
    //1 ako je puna, 2 ako nije
    int size = calculateSize(list);
    if(size==list->capacity)
        return 1;
    return 2;    

}

int main(){

    struct List* lista1 = initializeList(5,"borna");

    append(lista1, 3);
    append(lista1, 4);
    append(lista1, 5);
    append(lista1, 6);
    print(lista1);
    deleteNode(lista1, 0);
    print(lista1);
    isEmpty(lista1);

    printf("\n ...did it");


    return 0; 
}